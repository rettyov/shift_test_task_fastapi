# Тестовое задание

## Описание

Сервис состоит из основной веб части и базы данных postgres из docker-образа. 

Перед запуском сервиса для локального тестирования создаются две таблицы:
 - `employee`. содержит поля `name` (str), `surname`: (str), `salary` (int), `promotion` (datetime). 
 Личные данные пользователя.
 - `logauth`. содержит поля `username` (str), 'password' str, `employee_id` (reference from 'employee.employee_id').
Параметры входа в свою учётную запись.
 
Также создаются две записи в таблицах. Для всего вышеописанного вызывается в файле `main.py` функция `create_db`.

## Доступные 'ручки'

### post

- `/login`. Вход в аккаунт и получение токена. Принимает username и password. Возвращает access_token и refresh_token.

### get

- `/refresh_token`. Обновляет токен. Возвращает access_token.
- `/get-user-info`. Возвращает информацию о пользователе (name, surname, salary, promotion)

## Запускаем через Docker

```
docker-compose up
```
